var ChainTypes;

module.exports = ChainTypes = {};

ChainTypes.reserved_spaces = {
  relative_protocol_ids: 0,
  protocol_ids: 1,
  implementation_ids: 2
};

ChainTypes.operations= {
    vote: 0,
    comment: 1,
    transfer: 2,
    transfer_to_vesting: 3,
    withdraw_vesting: 4,
    account_create: 5,
    account_update: 6,
    witness_update: 7,
    account_witness_vote: 8,
    account_witness_proxy: 9,
    custom: 10,
    delete_comment: 11,
    custom_json: 12,
    comment_options: 13,
    set_withdraw_vesting_route: 14,
    limit_order_create2: 15,
    challenge_authority: 16,
    prove_authority: 17,
    request_account_recovery: 18,
    recover_account: 19,
    change_recovery_account: 20,
    escrow_transfer: 21,
    escrow_dispute: 22,
    escrow_release: 23,
    pow2: 24,
    escrow_approve: 25,
    transfer_to_savings: 26,
    transfer_from_savings: 27,
    cancel_transfer_from_savings: 28,
    custom_binary: 29,
    decline_voting_rights: 30,
    reset_account: 31,
    set_reset_account: 32,
    claim_reward_balance: 33,
    delegate_vesting_shares: 34,
    account_create_with_delegation: 35,
    fill_convert_request: 36,
    author_reward: 37,
    curation_reward: 38,
    comment_reward: 39,
    liquidity_reward: 40,
    interest: 41,
    fill_vesting_withdraw: 42,
    fill_order: 43,
    shutdown_witness: 44,
    fill_transfer_from_savings: 45,
    hardfork: 46,
    comment_payout_update: 47,
    return_vesting_delegation: 48,
    comment_benefactor_reward: 49
};

//types.hpp
ChainTypes.object_type = {
  "null": 0,
  base: 1,
};
